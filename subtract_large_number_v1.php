<?php
#Name:Add Large Number v1
#Description:Subtract a large number (such as those exceeding PHP max interger) from another large number. Does not calculate negative numbers. Calculation is the positive remainder, or zero. Returns the number as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'number1' (required) is a string containing the number. 'number2' (required) is a string containing the number to subtract from the first number. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):number1:string:required,number2:string:required,display_errors:bool:optional
#Content:
if (function_exists('subtract_large_number_v1') === FALSE){
function subtract_large_number_v1($number1, $number2, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if ($number1 == '0'){
		#Do nothing to avoid ctype_digit thinking 0 === FALSE
	} else if (@ctype_digit($number1) === FALSE){
		$errors[] = "number1";
	}
	if ($number2 == '0'){
		#Do nothing to avoid ctype_digit thinking 0 === FALSE
	} else if (@ctype_digit($number2) === FALSE){
		$errors[] = "number2";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Decrement $number1 $number2 times. Calculation is the positive remainder or zero. Do not calculate negative numbers.]
	if (@empty($errors) === TRUE){
		$location = realpath(dirname(__FILE__));
		do {
			require_once $location . '/dependencies/decrement_large_number_v1.php';
			$number1 = @decrement_large_number_v1($number1, FALSE);
			$number2 = @decrement_large_number_v1($number2, FALSE);
		} while ($number2 != '0' and $number1 != '0');
		###If $number1 is smaller than $number2, change $number1 to '0' to indicate there is no positive remainder. The previous while condition "$number1 != '0'" ensures a break occurs before $number2 reaches zero if $number1 is smaller.
		if ($number2 != '0'){
			$number1 = '0';
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('subtract_large_number_v1_format_error') === FALSE){
			function subtract_large_number_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("subtract_large_number_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $number1;
	} else {
		return FALSE;
	}
}
}
?>